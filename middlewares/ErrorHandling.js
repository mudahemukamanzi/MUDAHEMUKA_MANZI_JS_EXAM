const dbDebug = require('debug')('db:Errors')
exports.ErrorHandler = (err, req, res, next)=>{
    if(err){
        err = {
            statusCode: err.statusCode||500,
            message: err.message
        }
        dbDebug(`error ${err.message}`);
        return res.status(err.statusCode).json({msg: err.message});
    }
}