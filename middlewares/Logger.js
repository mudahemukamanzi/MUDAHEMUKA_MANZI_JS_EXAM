const debugApi = require('debug')('debug:api');
exports.Logger = (req, res, next)=>{
    if(req){
        debugApi(" ... Accessing apis ...");
        next();
    }
}