const mongoose = require("mongoose");
const config = require("config");
const dbDebug = require("debug")("db:start");
module.exports= mongoose.connect("mongodb://localhost/CountryManagement",{
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: true
})
.then(()=>{
   dbDebug(" ....Successfully connected to DB ..."+config.get('name'));
   console.log("connected to db")
})
.catch(err=>{
    console.log(err);
})
