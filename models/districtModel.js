const mongoose = require("mongoose");
const districtModel = new mongoose.Schema({
    name:{
        type: String,
        minLength: 4,
        maxLength: 255,
        required: true
    },
    dr_Id:{
        type: String,
        required: true
    },
    pr_Id:{
        type: String,
        required: true
    }
})

exports.District = mongoose.model("Districts", districtModel);