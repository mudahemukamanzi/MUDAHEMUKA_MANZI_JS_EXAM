const mongoose = require("mongoose");
const provinceModel = new mongoose.Schema({
    name:{
        type: String,
        minLength: 4,
        maxLength: 255,
        required: true
    },
    pr_Id:{
        type: String,
        required: true
    }
})

exports.Province = mongoose.model("Provinces", provinceModel);