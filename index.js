const express = require("express");
require("./models/mongodb");
const startDebug = require('debug')('app:debug');
const bodyParser = require("body-parser");
const config = require("config");
const provinceRoutes = require("./routes/provinceRoutes");
const districtRoutes = require("./routes/districtRoutes");
const { Logger } = require("./middlewares/Logger");
const { ErrorHandler } = require("./middlewares/ErrorHandling");
const app = express();
//
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}));

app.use("/api/provinces/", Logger, provinceRoutes);
//District endpoints
app.use("/api/districts/",Logger, districtRoutes);
//Error handling middle ware
app.use(ErrorHandler);
startDebug(" ...app "+config.get('name'));
startDebug(" Listening to port "+config.get('application.port'));
app.listen(config.get(application.port));
startDebug("...Listening to endpoint ...");
