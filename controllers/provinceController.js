const { Province } = require("../models/provinceModel");
const { validateProvince } = require("../utils/validate");

exports.getProvinces = async (req, res, next) =>{
       try {
             const provinces = await Province.find();
             if(!provinces) return res.json({msg: "No province found"});
             return res.json({provinces});
       } catch (err) {
          next(err);
       }
}
exports.createProvince = async (req, res, next)=>{
     try {
         const { name, pr_Id } = req.body;
         const { error } = validateProvince(req.body);
         if(error) return res.json({msg: error.details[0].message});
         const newprov = new Province({
             name,
             pr_Id
         });
         await newprov.save();
         return res.json({msg: "Created province"});
     } catch (err) {
        // ErrorHandler(err, req, res, next);
        console.log("done it perfectly")
     }
}

exports.updateProvince = async (req, res, next)=>{
    try {
        const {error } = validateProvince(req.body);
        if(error) return res.json({msg: error.details[0].message});
        const updateProv = await Province.findByIdAndUpdate({_dd: req.params.id}, req.body, {new: true});
        return res.json({updateProv});
    } catch (err) {
        next(err);
    }
}
exports.deleteProvince = async (req, res, next)=>{
    try {
         const prov = await Province.findByIdAndDelete({_id: req.params.id});
         if(!prov) return res.json({msg: "province with that Id is not found"});

        return res.json({msg: "Deleted province"});
    } catch (err) {
        next(err);
    }
}