const { District } = require("../models/districtModel");
const { validateDistrict} = require("../utils/validate");

exports.getDistricts = async (req, res, next) =>{
       try {
             const districts = await District.find();
             if(!districts) return res.json({msg: "No province found"});
             return res.json({districts});
       } catch (err) {
           next(err);
       }
}
exports.createDistrict = async (req, res, next)=>{
     try {
         const { name, pr_Id, dr_Id } = req.body;
         const {error } = validateDistrict(req.body);
         if(error) return res.json({msg: error.details[0].message});
         const newdistrict = new District({
             name,
             dr_Id,
             pr_Id
         });
         await newdistrict.save();
         return res.json({msg: "Created province"});
     } catch (err) {
         next(err);
     }
}

exports.updateDistrict = async (req, res, next)=>{
    try {
        const {error } = validateDistrict(req.body);
        if(error) return res.json({msg: error.details[0].message});
        const updatedistrict = await District.findByIdAndUpdate({_id: req.params.id}, req.body, {new: true});
        return res.json({updatedistrict});
    } catch (err) {
        next(err);
    }
}
exports.deleteDistrict = async (req, res, next)=>{
    try {
        await District.findByIdAndDelete({_Id: req.params.id});
        return res.json({msg: "Deleted province"});
    } catch (err) {
        next(err);
    }
}