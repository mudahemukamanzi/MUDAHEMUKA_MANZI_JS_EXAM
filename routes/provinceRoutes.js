const router=require('express').Router();
const { getProvinces, createProvince, updateProvince, deleteProvince } = require("../controllers/provinceController");
//get all provinces
router.get("/", getProvinces);
//creating new province
router.post("/", createProvince);
// update province
router.put("/:id", updateProvince);
//delete province 
router.delete("/:id", deleteProvince);
module.exports = router;