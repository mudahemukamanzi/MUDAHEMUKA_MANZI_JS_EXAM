const router= require('express').Router();
const { getDistricts, createDistrict, updateDistrict, deleteDistrict } = require("../controllers/districtController");
//get all provinces
router.get("/", getDistricts);
//creating new province
router.post("/", createDistrict);
//update district
router.put("/", updateDistrict);
//Delete district
router.delete("/", deleteDistrict);
module.exports = router;