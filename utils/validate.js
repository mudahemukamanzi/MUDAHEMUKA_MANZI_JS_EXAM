const Joi = require("joi");

exports.validateProvince = (data)=>{
    const Schema = Joi.object().keys({
        name: Joi.string().min(4).max(255).required(),
        pr_Id: Joi.string().min(3).max(255).required()
    })
    const result = Schema.validate(data);
    return result;
}

exports.validateDistrict = (data)=>{
    const Schema = Joi.object().keys({
        name: Joi.string().min(3).max(255).required(),
        dr_Id: Joi.string().min(3).max(255).required()
    })
    const result = Schema.validate(data);
    return result;
}